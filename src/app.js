import React, { Component } from "react";
import ReactDOM from "react-dom";
import ReduxThunk from 'redux-thunk';
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import promise from "redux-promise";
import reducers from "./reducers";
import Routes from './routes';

const createStoreWithMiddleware = applyMiddleware(promise)(createStore);

class App extends Component {

  render() {
    console.log('app',this);

    return (
      <Provider store={createStoreWithMiddleware(reducers, {}, applyMiddleware(ReduxThunk))}>
        <Routes />
      </Provider>
    );
  }
}

export default App;
