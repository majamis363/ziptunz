import React, { Component } from "react";
import ReactDOM from "react-dom";
import { BrowserRouter, Route, Switch } from "react-router-dom";

import Login from './components/login';
import Logout from './components/logout';

import P3 from "./components/p3";
import P4 from "./components/p4";
import P5 from "./components/p5";
import EnterVideo from "./components/enterVideo";
import Index from "./components/index";

import Header from './components/header';
import { onAuthStateChanged } from './actions';
import { Spinner } from '@blueprintjs/core';
import { connect } from 'react-redux';

class Routes extends Component {

  componentWillMount() {
      this.props.onAuthStateChanged();
  }

  render() {
    console.log(this);

    if(this.props.auth && this.props.auth.loading )
    {
      return (
        <div style={{ textAlign: "center", position: "absolute", top: "25%", left: "50%" }}>
          <h3>Loading</h3>
          <Spinner />
        </div>
      );
    }

    return (
        <BrowserRouter>
          <div>
            <Header />
            <Switch>
              <Route path="/index" component={Index} />
              <Route path="/p3" component={P3} />
              <Route path="/p4" component={P4} />
              <Route path="/p5" component={P5} />
              <Route path="/enterVideo" component={EnterVideo} />
              <Route path="/logout" component={Logout} />
              <Route path="/login" component={Login} />
              <Route path="/" component={Login} />
            </Switch>
          </div>
        </BrowserRouter>
    );
  }
}

const mapStateToProps = (state) => {
   return { auth: state.auth };
};

export default connect(mapStateToProps, { onAuthStateChanged })(Routes);
