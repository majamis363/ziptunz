import {
  LOGIN_USER_SUCCESS,
  LOGIN_USER_FAIL,
  LOGIN_USER_FAIL_FACEBOOK,
  LOGIN_USER_START
} from '../actions';

const INITIAL_STATE = {
  currentUser: null,
  authenticated: false,
  loading: true,
  error: null
};

export default (state = INITIAL_STATE, action) => {
  console.log(action.type,action.payload);
  switch (action.type) {
    case LOGIN_USER_SUCCESS:
      return { ...state, currentUser: action.payload, authenticated: true,
         loading: false, error: null };
    case LOGIN_USER_FAIL:
      return { ...state, currentUser: null, authenticated: false, loading: false,
         error: action.payload };
    case LOGIN_USER_START:
      return { ...state, loading: true, error: null };
    case LOGIN_USER_FAIL_FACEBOOK:
      return { ...state, currentUser: null, authenticated: false,
        error: 'Unable to sign in with Facebook', loading: false }
    default:
      return state;
  }
};
