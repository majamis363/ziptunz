import {
    LOAD_DATA,
    ADD_VIDEO_SUCCESS,
    ADD_VIDEO_START,
    ADD_VIDEO_FAIL
  } from '../actions';
  
  const INITIAL_STATE = {
    info: {},
    videos: null,
    uploading: false,
  };
  
  export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
      case LOAD_DATA:
        return { info: action.payload };
      case ADD_VIDEO_START:
        return { ...state, uploading: true };
      case ADD_VIDEO_SUCCESS:
        return { ...state, videos: action.payload, uploading: false };
      case ADD_VIDEO_FAIL:
        return { ...state, uploading: false };
      default:
        return state;
    }
  };
  