import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import AuthReducer from './reducer_auth';
import UserReducer from './reducer_user';

const rootReducer = combineReducers({
  auth: AuthReducer,
  form: formReducer,
  user: UserReducer
});

export default rootReducer;
