import React, { Component } from 'react'
import { Link } from 'react-router-dom';

const newSongStyles = {
  padding: '10px'
}

class NewVideoForm extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <div style={newSongStyles}>
        <Link className="pt-button pt-intent-primary" to="/enterVideo" aria-label="Create Video">Create Video</Link>
      </div>
    )
  }
}

export default NewVideoForm
