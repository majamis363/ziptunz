import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import _ from 'lodash';
import { app, base } from '../javascripts/firebase.js';
import { Field, reduxForm } from 'redux-form';

class Index extends Component {

  componentDidMount() {
    const { currentUser } = this.props.auth;
    base.syncState(`user/${currentUser.uid}`, {
      context: this,
      state: 'info',
      then() {
        this.props.initialize(this.state.info);
        this.setState({loading: false});
      }
    });
  }

  constructor() {
    super();
    this.state = {
      loading: true
    };
  }

  renderField(field) {
    const { meta: { touched, error } } = field;
    const className = `form-group ${touched && error ? 'has-danger' : '' }`;
    
    return (
      <div className={className}>
        <label>{field.label}</label>
        <input className="form-control"
          {...field.input}
        />
        <div className = "text-help">
          {touched ? error : ''}
        </div>
      </div>
    );
  }

  render() {
    const { handleSubmit } = this.props;

    if (!this.props.auth || this.props.auth.authenticated === false) {
      return <Redirect to='/login' />;
    }

    if (this.state.loading === true) {
      return <div />;
    }

    return(
      <div>
        
      </div>
    );
  }
}

const mapStateToProps = (state) => {
   return { auth: state.auth };
};

function validate(values) {
  const errors = {};

  // validate inputs from 'values'
  if(!values.name) {
    errors.name = "Enter a Name!";
  }
  else if (values.name.length < 3 ) {
    errors.name = "Name must be atleast 3 charectors";
  }

  if(!values.zipCode) {
    errors.zipCode = "Enter Zip Code!";
  }

  // If errors is empty, the form is fine to submit
  // If errors has *any* properties redux form assumes form is invalid
  return errors;
}

export default reduxForm({
  validate,
  form: 'PostsNewForm',
  enableReinitialize: true

})(
  connect(mapStateToProps,{ })(Index)
);
