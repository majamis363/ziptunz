import React, { Component } from 'react'
import { connect } from 'react-redux';

const newSongStyles = {
  padding: '10px'
}

class UserInfo extends Component {
  constructor(props) {
    super(props);
  }


  render() {
    console.log(this);
    if(!this.props.info)
        return <div></div>;

    return (
      <div style={newSongStyles}>
        <div>
            <div>
                <div>{this.props.info.name}</div>
                <div >{this.props.auth.currentUser.email}</div>
            </div>
        </div>
      </div>
    )

  }
}

export default UserInfo
