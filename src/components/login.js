import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link, Redirect } from 'react-router-dom';
import { Field, reduxForm } from 'redux-form';
import { Toaster, Intent } from '@blueprintjs/core';
import { authWithFacebook, authWithEmailPassword } from '../actions'

class Login extends Component {

  constructor(props) {
    super(props);
    this.state = {
      redirect: false
    }
  }

  renderField(field) {
    const { meta: { touched, error } } = field;
    const className = `form-group ${touched && error ? 'has-danger' : '' }`;

    return (
      <div className={className}>
        <label>{field.label}</label>
        <input type={field.type} placeholder={field.placeholder} className="form-control"
          {...field.input}
        />
        <div className = "text-help">
          {touched ? error : ''}
        </div>
      </div>
    );
  }

  onSubmit(values) {
    this.props.authWithEmailPassword(values);
  }

  render() {
    const { handleSubmit } = this.props;
    const { from } = this.props.location.state || { from: { pathname: '/index' } };

    if (this.props.auth && this.props.auth.error && this.toaster) {
      this.toaster.show({ intent: Intent.DANGER, message: this.props.auth.error.message })
      this.props.auth.error = null;
    }

    if (this.props.auth && this.props.auth.authenticated === true && this.toaster) {
      this.toaster.show({ intent: Intent.SUCCESS, message: 'Log in successful',  onDismiss: () => this.setState({ redirect: true }) });
      this.loginForm.reset();
    }

    if (this.props.auth && this.props.auth.authenticated === true) {
      return <Redirect to={from} />;
    }

    if (this.state.redirect === true ) {
      return <Redirect to={from} />;
    }

    return(
      <div>
        <Toaster ref={(element) => { this.toaster = element }} />
        <div className="row">
          <div className="col">
            <div className="my-5">
              <img alt="logo" src="/assets/img/logo.png" className="rounded center-block d-block"/>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col">
            <div className="well center-block-login">
              <form onSubmit={handleSubmit(this.onSubmit.bind(this))} ref={(form) => { this.loginForm = form }}>
                <Field
                  name="email"
                  label="Email"
                  type="email"
                  placeholder="Email"
                  ref={(input) => { this.emailInput = input }}
                  component={this.renderField}
                />
                <Field
                  name="password"
                  label="Password"
                  type="password"
                  placeholder="Password"
                  ref={(input) => { this.passwordInput = input }}
                  component={this.renderField}
                />
                <button type="submit" className="btn btn-block btn-lg btn-default">Login</button>
              </form>
              <hr style={{marginTop: "10px", marginBottom: "10px"}}/>
              <button onClick={() => { this.props.authWithFacebook() }} className="my-3 btn btn-primary btn-lg btn-block" >
                Login With Facebook
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
   return { auth: state.auth };
};

function validate(values) {
  const errors = {};
  // validate inputs from 'values'

  // If errors is empty, the form is fine to submit
  // If errors has *any* properties redux form assumes form is invalid
  return errors;
}

export default reduxForm({
  validate,
  form: 'LoginForm'
})(
  connect(mapStateToProps,{ authWithFacebook, authWithEmailPassword })(Login)
);
