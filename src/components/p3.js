import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import _ from 'lodash';
import { app, base } from '../javascripts/firebase.js';
import { Field, reduxForm } from 'redux-form';

class P3 extends Component {

  componentDidMount() {
    const { currentUser } = this.props.auth;
    base.syncState(`user/${currentUser.uid}`, {
      context: this,
      state: 'info',
      then() {
        this.props.initialize(this.state.info);
        this.setState({loading: false});
      }
    });
  }

  constructor() {
    super();
    this.state = {
      loading: true
    };
  }

  renderField(field) {
    const { meta: { touched, error } } = field;
    const className = `form-group ${touched && error ? 'has-danger' : '' }`;
    
    return (
      <div className={className}>
        <label>{field.label}</label>
        <input className="form-control"
          {...field.input}
        />
        <div className = "text-help">
          {touched ? error : ''}
        </div>
      </div>
    );
  }

  onSubmit(values) {
    console.log("values",values);
    this.props.history.push('/p4');
    this.setState({
        info: _.assign(this.state.info,values) //updates Firebase and the local state
    });
  }

  render() {
    const { handleSubmit } = this.props;

    if (!this.props.auth || this.props.auth.authenticated === false) {
      return <Redirect to='/login' />;
    }


    if (this.state.loading === true) {
      return <div />;
    }

    return(
      <div>
        <div className="row">
          <div className="col">
            <div className="my-5">
              <img src="/assets/img/camera.png" className="rounded center-block d-block"/>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col">
            <div className="well center-block">
              <form onSubmit={handleSubmit(this.onSubmit.bind(this))}>
                <Field
                  name="name"
                  label="Name"
                  component={this.renderField}
                />
                <div className="row">
                  <div className="col">
                    <label>Are you an artist?</label>
                  </div>
                  <div className="col">
                    <div className="row">
                      <label className="my-2">
                        <Field
                          name="artist"
                          component="input"
                          type="radio"
                          value="yes"
                        />{' '}
                        Yes
                      </label>
                    </div>
                    <div className="row">
                      <label className="my-2">
                        <Field
                          name="artist"
                          component="input"
                          type="radio"
                          value="no"
                        />{' '}
                        No
                      </label>
                    </div>
                  </div>
                </div>
                <Field
                  name="zipCode"
                  label="Zip Code"
                  component={this.renderField}
                />
                <button type="submit" className="btn btn-block btn-lg btn-primary">Continue</button>

              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
   return { auth: state.auth };
};

function validate(values) {
  const errors = {};

  // validate inputs from 'values'
  if(!values.name) {
    errors.name = "Enter a Name!";
  }
  else if (values.name.length < 3 ) {
    errors.name = "Name must be atleast 3 charectors";
  }

  if(!values.zipCode) {
    errors.zipCode = "Enter Zip Code!";
  }

  // If errors is empty, the form is fine to submit
  // If errors has *any* properties redux form assumes form is invalid
  return errors;
}

export default reduxForm({
  validate,
  form: 'PostsNewForm'
})(
  connect(mapStateToProps,{ })(P3)
);
