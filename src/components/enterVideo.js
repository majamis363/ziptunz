import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import _ from 'lodash';
import { app, base } from '../javascripts/firebase.js';
import { Field, reduxForm } from 'redux-form';
import { Toaster, Intent } from '@blueprintjs/core';
import { addVideo } from '../actions';

class EnterVideo extends Component {

  componentDidMount() {
    const { currentUser } = this.props.auth;
    base.fetch(`user/${currentUser.uid}`, {
      context: this,
      then(data){
        this.setState({loading: false, userInfo: data});
      }
    });
  }

  constructor() {
    super();
    this.state = { loading: true, screen: "first", file: null };
  }

  renderField(field) {
    const { meta: { touched, error } } = field;
    const className = `form-group form-inline ${touched && error ? 'has-danger' : '' }`;

    return (
      <div className={className}>
        <label>{field.label}</label>
        <div className="p-1">-</div>
        <input placeholder={field.placeholder} className="form-control"
          {...field.input}
        />
        <div className = "col text-help">
          {touched ? error : ''}
        </div>
      </div>
    );
  }

  
  onChange(e) {
    e.preventDefault();
    const files = [ ...e.target.files ];
    if(files.length > 0)
      this.setState({ file:files[0] });
  }

  onSubmit(values) {

    if(this.state.screen === "first") 
    {
      if(values.video && values.video === "yes")
      {
        this.setState({ screen: "second" });
      }
      else if(values.video && values.video === "no")
        this.props.history.push('/');
    }
    else if(this.state.screen === "second")
    {
      this.setState({ screen: "third" });
    }
    else if(this.state.screen === "third")
    {
      if(this.state.file) {
        values = _.assign(values,this.state);
        this.props.addVideo(values);
        this.toaster.show({ intent: Intent.SUCCESS, message: "Uploaded a video successfully!" });
      }
      else
        this.toaster.show({ intent: Intent.FAIL, message: "no video selected" });
    }
  }

  renderFirstScreen() {
    const { handleSubmit } = this.props;

    return(
      <form onSubmit={handleSubmit(this.onSubmit.bind(this))}>
        <div className="row my-3">
          <div className="col">
            <div className="row">
              <div className="col">
                <label>Voting schedule | (Countdown time remaining until Sat, 8pm) left to enter video</label>
              </div>
            </div>
            <div className="row">
              <div className="col">
                <img src="/assets/img/voting.png" className="rounded img-fluid"/>
              </div>
            </div>
          </div>
        </div>
        <div className="row my-3">
          <div className="col">
            <label>Do you want to enter a video?</label>
          </div>
          <div className="col">
            <div className="row px-4">
              <div className="col">
                <label className="my-2">
                  <Field
                    name="video"
                    component="input"
                    type="radio"
                    value="yes"
                  />{' '}
                  Yes
                </label>
              </div>
              <div className="col">
                <label className="my-2">
                  <Field
                    name="video"
                    component="input"
                    type="radio"
                    value="no"
                  />{' '}
                  No
                </label>
              </div>
            </div>
          </div>
        </div>
        <div className="row my-3">
          <div className="col">
            <button type="submit" className="btn btn-block btn-lg btn-primary">Continue</button>
          </div>
        </div>
      </form>
    );
  }

  renderSecondScreen() {
    const { handleSubmit } = this.props;

    return(
      <form onSubmit={handleSubmit(this.onSubmit.bind(this))}>
        <div className="row my-3">
          <div className="col">
            <div className="row align-items-center">
              <div className = "col">
                <p className="text-center">Would you like to ...</p>
              </div>
            </div>
            <div className="row align-items-center">
              <div className="col">
              <button>
                <img src="/assets/img/record.png" className="rounded img-fluid"/>
              </button>
              </div>
              <div className="col">
                <p className="text-center"> or </p>
              </div>
              <div className="col">
                <button>
                  <img src="/assets/img/upload.png" className="rounded img-fluid"/>
                </button>
              </div>
            </div>
          </div>
        </div>
        <div className="row my-3">
        
        </div>
      </form>
    );
  }

  renderThirdScreen() {
    const { handleSubmit } = this.props;

    return(
      <form onSubmit={handleSubmit(this.onSubmit.bind(this))}>
        <div className="row my-3">
          <div className="col">
            <div className="row">
              <div className="col">
                <label>Thumbnail of video |</label>
              </div>
            </div>
            <div className="row">
              <div className="col">
                <Field
                  name="thumbnail"
                  label="Add Thumbnail"
                  component="textarea"
                  className="form-control"
                />
              </div>
            </div>
          </div>
        </div>
        <div className="row my-3">
          <div className="col">
            <Field
              name="title"
              label={this.state.userInfo.name}
              placeholder="title"
              component={this.renderField}
            />
          </div>
        </div>
        <div className="row my-3">
          <div className="col">
            <input name="video" type="file" accept=".mp4" onChange={this.onChange.bind(this)} />
          </div>
        </div>
        <div className="row my-3">
          <div className="col">
            <button type="submit" className="btn btn-block btn-lg btn-primary">Continue</button>
          </div>
        </div>
      </form>
    );
  }

  renderForm() {
    if (this.state.screen === "first")
      return this.renderFirstScreen();
    else if (this.state.screen === "second")
      return this.renderSecondScreen();
    else if(this.state.screen === "third")
      return this.renderThirdScreen();
  }

  render() {

    if (!this.props.auth || this.props.auth.authenticated === false) {
      return <Redirect to='/login' />;
    }
    
    if (this.state.loading)
      return <div> </div>;

    return(
      <div>
        <Toaster ref={(element) => { this.toaster = element }} />
        <div className="row" style={ { height:'160px' } } />
        <div className="row">
          <div className="col">
            <div className="well center-block">
              <div className="col">
              { 
                  this.renderForm()
              }
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function validate(values) {
  const errors = {};

  // validate inputs from 'values'
  if(!values.title) {
    errors.title = "Enter a title!";
  }

  // If errors is empty, the form is fine to submit
  // If errors has *any* properties redux form assumes form is invalid
  return errors;
}

const mapStateToProps = (state) => {
   return { auth: state.auth };
};

export default reduxForm({
  validate,
  form: 'PostsNewForm'
})(
  connect(mapStateToProps,{ addVideo })(EnterVideo)
);
