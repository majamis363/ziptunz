import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link, Redirect } from 'react-router-dom';
import _ from 'lodash';
import { app, base } from '../javascripts/firebase.js';
import { Field, reduxForm } from 'redux-form';

class P4 extends Component {

  componentDidMount() {
    const { currentUser } = this.props.auth;
    base.syncState(`user/${currentUser.uid}`, {
      context: this,
      state: 'info',
      then() {
        this.props.initialize(this.state.info);
        this.setState({loading: false});
      }
    });
  }

  constructor() {
    super();
    this.state = {
      loading: true
    };
  }

  renderField(field) {
    const { meta: { touched, error } } = field;
    const className = `form-group ${touched && error ? 'has-danger' : '' }`;

    return (
      <div className={className}>
        <label>{field.label}</label>
        <input placeholder={field.placeholder} className="form-control"
          {...field.input}
        />
        <div className = "text-help">
          {touched ? error : ''}
        </div>
      </div>
    );
  }

  onSubmit(values) {
    this.props.history.push('/p5');
    this.setState({
      info: _.assign(this.state.info,values) //updates Firebase and the local state
  });
  }

  render() {
    const { handleSubmit } = this.props;

    if (!this.props.auth || this.props.auth.authenticated === false) {
      return <Redirect to='/login' />;
    }

    if (this.state.loading === true) {
      return <div />;
    }

    return(
      <div>
        <div className="row" style={ { height:'160px' } } />
        <div className="row">
          <div className="col">
            <div className="well center-block">
              <form onSubmit={handleSubmit(this.onSubmit.bind(this))}>
                <div className="row my-3">
                  <div className="col">
                    <Field
                      name="userName"
                      label="Welcome to Ziptunz, (Enter username)"
                      component={this.renderField}
                      type="text"
                      className="form-control"
                    />
                  </div>
                </div>
                <div className="row my-3">
                  <div className="col">
                    <label>Are a part of a group?</label>
                  </div>
                  <div className="col">
                    <div className="row px-4">
                      <div className="col">
                        <label className="my-2">
                          <Field
                            name="group"
                            component="input"
                            type="radio"
                            value="yes"
                          />{' '}
                          Yes
                        </label>
                      </div>
                      <div className="col">
                        <label className="my-2">
                          <Field
                            name="group"
                            component="input"
                            type="radio"
                            value="no"
                          />{' '}
                          No
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="row my-3">
                  <div className="col">
                    <Field
                      name="roll"
                      label="What is your roll? (drummer, vocals, etc)"
                      component={this.renderField}
                    />
                  </div>
                </div>
                {/*<Field
                  name="content"
                  label="Content"
                  component={this.renderField}
                />*/}
                <div className="row my-3">
                  <div className="col">
                    <Link to="/p4" className="btn btn-block btn-lg btn-default">+ Add Members</Link>
                  </div>
                </div>
                <div className="row my-3">
                  <div className="col">
                    <button type="submit" className="btn btn-block btn-lg btn-primary">Continue</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function validate(values) {
  const errors = {};

  // validate inputs from 'values'
  if(!values.userName) {
    errors.userName = "Enter a username!";
  }
  else if (values.userName.length < 3 ) {
    errors.userName = "username must be atleast 3 charectors";
  }

  if(!values.roll) {
    errors.roll = "Enter Roll!";
  }

  // If errors is empty, the form is fine to submit
  // If errors has *any* properties redux form assumes form is invalid
  return errors;
}

const mapStateToProps = (state) => {
   return { auth: state.auth };
};

export default reduxForm({
  validate,
  form: 'PostsNewForm'
})(
  connect(mapStateToProps,{  })(P4)
);
