import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { Popover, PopoverInteractionKind, Position } from '@blueprintjs/core'
import { connect } from 'react-redux';
import NewVideoForm from './newVideoForm';
import UserInfo from './userInfo';
import { addVideo } from '../actions';
import { app, base } from '../javascripts/firebase.js';

class Header extends Component {

  componentWillMount() {
    const { currentUser } = this.props.auth;
      if(this.props.auth.authenticated) {
      base.fetch(`user/${currentUser.uid}`, {
        context: this,
        then(data){
          this.setState({loading: false, userInfo: data});
        }
      });
    }
  }

  constructor(props) {
    super(props)

    this.closePopover = this.closePopover.bind(this)
    this.closeUserInfoPopOver = this.closeUserInfoPopOver.bind(this)
    this.state = {
      popoverOpen: false,
      userInfoPopOver: false
    }
  }

  closePopover() {
    this.setState({ popoverOpen: false })
  }

  closeUserInfoPopOver() {
    this.setState({ userInfoPopOver: false })
  }

  changeSettings() {
    return <Redirect to='/login' />;
  }

  render() {
    console.log(this);
    return (
      <nav className="pt-navbar">
        <div className="pt-navbar-group pt-align-left">
          <div className="pt-navbar-heading">Ziptunz</div>
          {this.props.auth.authenticated
              ? <input className="pt-input" placeholder="Search Videos..." type="text" />
              : null
          }
        </div>
        {
          this.props.auth.authenticated
          ? (
            <div className="pt-navbar-group pt-align-right">
              <Link className="pt-button pt-minimal pt-icon-video" to="/songs">Videos</Link>
              <Link className="pt-button pt-minimal pt-icon-add" to="/enterVideo" aria-label="Settings"></Link>
              {
                /*
                <Popover
                content={(<NewVideoForm postSubmitHandler={this.closePopover} />)}
                interactionKind={PopoverInteractionKind.CLICK}
                isOpen={this.state.popoverOpen}
                onInteraction={(state) => this.setState({ popoverOpen: state })}
                position={Position.BOTTOM}>
                <button className="pt-button pt-minimal pt-icon-add" aria-label="add new song"></button>
                </Popover>
                */
              }
              <span className="pt-navbar-divider"></span>
              <Popover
                content={(<UserInfo auth={this.props.auth} info={this.state.userInfo} postSubmitHandler={this.closeUserInfoPopOver} />)}
                interactionKind={PopoverInteractionKind.CLICK}
                isOpen={this.state.userInfoPopOver}
                onInteraction={(state) => this.setState({ userInfoPopOver: state })}
                position={Position.BOTTOM}>
                <button className="pt-button pt-minimal pt-icon-user" aria-label="User Info"></button>
              </Popover>
              <Link className="pt-button pt-minimal pt-icon-cog" to="/p3" aria-label="Settings"></Link>
              <Link className="pt-button pt-minimal pt-icon-log-out" to="/logout" aria-label="Log Out"></Link>
            </div>
          )
            : (
              <div className="pt-navbar-group pt-align-right">
                <Link className="pt-button pt-intent-primary" to="/login">Login</Link>
              </div>
            )
        }
      </nav>
    );
  }
}

const mapStateToProps = (state) => {
   return { auth: state.auth };
};

export default connect(mapStateToProps, { addVideo } )(Header);
