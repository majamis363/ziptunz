import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link, Redirect } from 'react-router-dom';
import _ from 'lodash';
import { Field, reduxForm } from 'redux-form';
import { app, base } from '../javascripts/firebase.js';

class P5 extends Component {

  componentDidMount() {
    const { currentUser } = this.props.auth;
    base.syncState(`user/${currentUser.uid}`, {
      context: this,
      state: 'info',
      then() {
        this.props.initialize(this.state.info);
        this.setState({loading: false});
      }
    });
  }

  constructor() {
    super();
    this.state = {
      loading: true
    };
  }

  renderField(field) {
    const { meta: { touched, error } } = field;
    const className = `form-group ${touched && error ? 'has-danger' : '' }`;

    return (
      <div className={className}>
        <label>{field.label}</label>
        <input placeholder={field.placeholder} className="form-control"
          {...field.input}
        />
        <div className = "text-help">
          {touched ? error : ''}
        </div>
      </div>
    );
  }

  onSubmit(values) {
    this.props.history.push('/p6');
    this.setState({
      info: _.assign(this.state.info,values) //updates Firebase and the local state
    });
  }

  render() {
    const { handleSubmit } = this.props;

    if (!this.props.auth || this.props.auth.authenticated === false) {
      return <Redirect to='/login' />;
    }

    if (this.state.loading === true) {
      return <div />;
    }
    
    return(
      <div>
        <div className="row" style={ { height:'160px' } } />
        <div className="row">
          <div className="col">
            <div className="well center-block">
              <form onSubmit={handleSubmit(this.onSubmit.bind(this))}>
                <div className="row my-3">
                  <div className="col">
                    <div className="row">
                      <div className="col">
                        <label>Add Bio |</label>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col">
                        <Field
                          name="bio"
                          label="Add bio"
                          component="textarea"
                          className="form-control"
                        />
                      </div>
                    </div>
                    <label>Write something...</label>
                  </div>
                </div>
                <div className="row my-3">
                  <div className="col">
                    <button type="submit" className="btn btn-block btn-lg btn-primary">Continue</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function validate(values) {
  const errors = {};

  if(!values.bio) {
    errors.bio = "Write something..";
  }
  return errors;
}

const mapStateToProps = (state) => {
   return { auth: state.auth };
};

export default reduxForm({
  validate,
  form: 'PostsNewForm'
})(
  connect(mapStateToProps,{  })(P5)
);
