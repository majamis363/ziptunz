import { app, facebookProvider } from '../javascripts/firebase.js';

export const LOGIN_USER_START = 'login_user_start';
export const LOGIN_USER_FAIL = 'login_user_fail';
export const LOGIN_USER_SUCCESS = 'login_user_success';
export const LOGIN_USER_FAIL_FACEBOOK = 'login_user_fail_facebook';

export const onAuthStateChanged = () => {
  return (dispatch) => {
    app.auth().onAuthStateChanged((user) => {
      if (user) {
        loginUserSuccess(dispatch,user);
        /*this.songsRef = base.syncState(`songs/${user.uid}`, {
          context: this,
          state: 'songs'
        });*/
      } else {
        loginUserFail(dispatch, null);
        //base.removeBinding(this.songsRef);
      }
    });
  };
};

export const authWithFacebook = () => {
  return (dispatch) => {
    app.auth().signInWithPopup(facebookProvider)
      .then((user, error) => {
        if (error) {
          loginUserFail(dispatch, { message: 'Unable to sign in with Facebook'});
          //this.toaster.show({ intent: Intent.DANGER, message: "Unable to sign in with Facebook" })
        } else {
          //loginUserSuccess(dispatch, user);
        }
      })
      .catch((error) => {
        loginUserFail(dispatch, error);
      });
  };
};

export const authWithEmailPassword = ( { email, password } ) => {

  return (dispatch) => {
    app.auth().fetchProvidersForEmail(email)
      .then((providers) => {
        if (providers.length === 0) {
          // create user
          return app.auth().createUserWithEmailAndPassword(email, password);
        } else if (providers.indexOf("password") === -1) {
          loginUserFail(dispatch,{ message: "Try alternative login."});
          // they used facebook
          // this.toaster.show({ intent: Intent.WARNING, message: "Try alternative login." });
        } else {
          // sign user in
          return app.auth().signInWithEmailAndPassword(email, password);

        }
      })
      .then((user) => {
        if (user && user.email) {
          //loginUserSuccess(dispatch, user);
        }
      })
      .catch((error) => {
        loginUserFail(dispatch, error);
      });
    };
};

const loginUserFailFacebook = (dispatch) => {
  dispatch({
    type: LOGIN_USER_FAIL_FACEBOOK,
    payload: null
  });
};

const loginUserFail = (dispatch, error) => {
  dispatch({
    type: LOGIN_USER_FAIL,
    payload: error
  });
};

const loginUserSuccess = (dispatch, user) => {
  dispatch({
    type: LOGIN_USER_SUCCESS,
    payload: user
  });
};
