import { app } from '../javascripts/firebase.js';

const USER_DETAIL_UPDATE = 'user_detail_update';
const USER_DETAIL_FETCH = 'user_detail_fetch';
const DETAIL_FETCH_SUCCESS = 'detail_fetch_success';
const DETAIL_SAVE_SUCCESS = 'detail_save_success';
const DETAIL_FETCH_FAIL = 'detail_fetch_fail';
export const ADD_VIDEO_SUCCESS = 'add_video_success';
export const ADD_VIDEO_START = 'add_video_start';
export const ADD_VIDEO_FAIL = 'add_video_fail';
export const LOAD_DATA = 'load_data';

export const userDetailUpdate = ({ prop, value }) => {
  return {
    type: USER_DETAIL_UPDATE,
    payload: { prop, value }
  };
};

export const addVideo = ({ thumbnail, title, file }) => {
  return (dispatch) => {
      const { currentUser } = app.auth();
      const storage = app.storage().ref(`/videos/${currentUser.uid}/${title}`);
      dispatch({ type: ADD_VIDEO_START, payload: null});
      storage.put(file).then(function(snapshot) {
        storage.getDownloadURL().then(function(url) {
          const key = app.database().ref(`/videos/${currentUser.uid}/${title}`)
          .set({ thumbnail, title, url });
          dispatch({ type: ADD_VIDEO_SUCCESS, payload: url});
        });
      })
      .catch((error) => {
        dispatch({ type: ADD_VIDEO_FAIL, payload: null});
        console.log("Action: addVideo",error);
      })
    
  }
};

export const loadData = ( data ) => {
  return (dispatch) => {
    dispatch({ type: LOAD_DATA, payload: data});
  }
}

export const detailSave = ({ name, gender, nationality, age }) => {
  const { currentUser } = app.auth();
  console.log("detailSave:",name,gender,nationality,age);
  return (dispatch) => {
    app.database().ref(`/users/${currentUser.uid}/`)
      .set({ name, gender, nationality, age })
      .then(() => {
        dispatch({ type: DETAIL_SAVE_SUCCESS });
      });
  };
};

export const detailFetch = () => {
  const { currentUser } = app.auth();
  return (dispatch) => {
    app.database().ref(`/users/${currentUser.uid}/`)
      .on('value', snapshot => {
        if (snapshot.val() !== null) {
          dispatch({ type: DETAIL_FETCH_SUCCESS, payload: snapshot.val() });
        } else {
          dispatch({ type: DETAIL_FETCH_FAIL });
        }
      });
  };
};
